<?php

//Obetener data en array associativo por consulta sql y conexion db 
//PARA 1 SOLO REGISTRO
function getArrayDataDB($conection, $sql){

	$execute_query = mysqli_query($conection, $sql);
	$result = array();

	foreach (mysqli_fetch_array($execute_query, MYSQLI_ASSOC) as $key => $value) {
		if ($key != 'password'){
			$result[$key] = $value;
		}
	}

	// mysqli_close($conection);
	return $result;
}

//Obetener data en array de arrays associativo por consulta sql y conexion db
//PARA MAS DE 1 REGISTRO
function getFetchAllDataDB($conection, $sql){

    $execute_query = mysqli_query($conection, $sql);

    if(!$execute_query) {
        error_log("\nUnable to process query, got error: " . mysqli_error($conection) . "\nQuery: " . $sql . get_backtrace_info());
        return false;
    }
    if(mysqli_num_rows($execute_query) > 0) {
        //RESULT SUPPLIED
        $row_array = array();

        while($row = mysqli_fetch_assoc($execute_query)) {
            $row_array[] = $row;
        }

        mysqli_free_result($execute_query);
        // mysqli_close($conection);

        return $row_array;
    }
    return false;
}

//Insertar sql y devolver id y msg
function insertDataDB($conection, $sql){

	$new_id = 0;
	$code_error = 0;
	$msg = '';

	if (mysqli_query($conection, $sql) === true) {
	    $new_id = mysqli_insert_id($conection);
	    $msg = 'Registro insertado con éxito';
	    $code_error = 0;
	} else {
		$new_id = 0;
	    $msg = "Error: " . mysqli_error($conection);
	    $code_error = mysqli_errno($conection);
	}

	// mysqli_close($conection);

  	$arr = array(
              'msg' => $msg, 
              'id' => $new_id,
              'code_error' => $code_error
      	);

  	return $arr;
}

//Update sql y devolver id y msg
function updateDataDB($conection, $sql){

    $code_error = 0;
    $msg = '';

    if (mysqli_query($conection, $sql) === true) {
        $msg = 'Registro updateado con éxito';
        $code_error = 0;
    } else {
        $msg = "Error: " . mysqli_error($conection);
        $code_error = mysqli_errno($conection);
    }

    // mysqli_close($conection);

    $arr = array(
              'msg' => $msg, 
              'code_error' => $code_error
    );

    return $arr;
}

//Eliminar sql y devolver msg
function deleteDataDB($conection, $sql){

    $code_error = 0;
    $msg = '';

    if (mysqli_query($conection, $sql) === true) {
        $msg = 'Registro eliminado con éxito';
        $code_error = 0;
    } else {
        $msg = "Error: " . mysqli_error($conection);
        $code_error = mysqli_errno($conection);
    }

    // mysqli_close($conection);

    $arr = array(
              'msg' => $msg, 
              'code_error' => $code_error
    );

    return $arr;
}

//Function cargar Select Generica
	function cargarSelect($value, $display, $table, $firstOption = 'Seleccione'){
    include('../bd/conexion.php');
    ?>
        <option value="0"><?php echo $firstOption; ?></option>
    <?php

    $sSQL = "SELECT ".$value." as value, ".$display." as display FROM ".$table." ORDER BY ".$display." ASC";

    $query = $conection->query($sSQL);

    $result = array();
    $i = 0;

    while ($row = mysqli_fetch_array($query)) {
        $displays = explode('-', $row['display']); 
        if ($displays[1] == ' ' || $displays[1] == ''){
            $row['display'] = $displays[0];
        }
        
        $result[] = array(
                0 => $row['value'],
                1 => $row['display']
        );

        ?>
            <option value="<?php echo $result[$i][0]; ?>"><?php echo $result[$i][1]; ?></option>
        <?php
        $i = $i +1;
    }

   // $query->close();
   $conection->next_result();
}      


?>
<?php
include_once 'includes/conexion.php';
include_once 'includes/funciones.php';

$action = (isset($_GET['action'])) ? $_GET['action'] : '';

switch ($action) {

	case 'select':
			$id_user = (isset($_GET['id_user'])) ? $_GET['id_user'] : '';
			$type_user = (isset($_GET['type_user'])) ? $_GET['type_user'] : '';
			
			//Si el parametro id viene entonces devuelve solo un registro de cliente con sus datos
			$where_for_edit = false;
			$id_client = (isset($_GET['id'])) ? $_GET['id'] : 0;
			if ($id_client != 0){
				$where_for_edit = true;
			}
			if ($type_user == 1 || $type_user == 3){

				//PARA TRAER DATOS DE CUENTA DE UN CLIENTE
				if ($where_for_edit == true){
					$sql = "SELECT c.*, u.name, u.lastname, u.status_cuenta_corriente
							FROM cuentas_corrientes c 
							LEFT JOIN users u ON c.id_client = u.id
							WHERE u.status = 'on'
							AND u.id = '$id_client' ";

				//PARA TRAER TODAS LAS CUENTAS
				}else{
					//FILTROS
					$filtro_cuenta = (isset($_GET['filtro_cuenta'])) ? $_GET['filtro_cuenta'] : '';

					$sql = "SELECT c.*, u.name, u.lastname, u.status_cuenta_corriente
							FROM cuentas_corrientes c 
							LEFT JOIN users u ON c.id_client = u.id
							WHERE u.status = 'on' ";

					//Si el filtro_cuenta viene distinto de vacio lo agrega a la sql.
					if ($filtro_cuenta != ''){
						$sql .= " AND (c.empresa LIKE '%".$filtro_cuenta."%' ";
						$sql .= " OR c.responsable LIKE '%".$filtro_cuenta."%' ";
						$sql .= " OR CONCAT(u.lastname, ' ', u.name) LIKE '%".$filtro_cuenta."%' ";
						$sql .= " OR CONCAT(u.name, ' ', u.lastname) LIKE '%".$filtro_cuenta."%') ";
					}

					$sql .= " GROUP BY u.id ";
				}

				$result = getFetchAllDataDB($oConexion, $sql);

				echo $_GET['jsoncallback'] . "(" . json_encode($result) . " ) ";
			}
		break;

	case 'insert_update_cuenta_corriente':
			$id_client = (isset($_GET['id_client'])) ? $_GET['id_client'] : '';
			$responsable = (isset($_GET['responsable'])) ? $_GET['responsable'] : '';
			$empresa = (isset($_GET['empresa'])) ? $_GET['empresa'] : '';
			$tipo_cuenta = (isset($_GET['tipo_cuenta'])) ? $_GET['tipo_cuenta'] : '';

			$status_cuenta_corriente = (isset($_GET['status_cuenta_corriente'])) ? $_GET['status_cuenta_corriente'] : '';

			$msg = '';
			$action = '';

			//Si el estado de la cuenta corriente es 0, se hace un insert y se cambia a 1
			if ($status_cuenta_corriente == '0'){
				$sql_insert = "INSERT INTO cuentas_corrientes (
						id_client, 
						responsable, 
						empresa,
						tipo_cuenta 
					) 
					VALUES (
						'$id_client', 
						'$responsable', 
						'$empresa', 
						'$tipo_cuenta'
					)";

				$insert_cuenta = insertDataDB($oConexion, $sql_insert);

				//Update a 1 status cuenta corriente en usuarios
				$sql_update_status_cuenta = "UPDATE users
					SET status_cuenta_corriente = '1'
					WHERE id = " . $id_client . "";

				$update_user_status_cuenta = updateDataDB($oConexion, $sql_update_status_cuenta);

				$msg = 'Nueva Cuenta Corriente Creada';
				$action = 'insert';

			//Si el estado de la cuenta corriente es 1, se hace un update y se mantiene el estado
			}else if ($status_cuenta_corriente == '1'){

				$sql_update = "UPDATE cuentas_corrientes
					SET responsable = '" . $responsable . "',
					empresa = '" . $empresa . "',
					tipo_cuenta = '" . $tipo_cuenta . "'
					WHERE id_client = " . $id_client . "";

				$update_cuenta = updateDataDB($oConexion, $sql_update);

				$msg = 'Cuenta Corriente Actualizada';
				$action = $sql_update;
			}

		  	$arr_result = array(
	              'msg' => $msg, 
	              'action' => $action
	      	);

			echo $_GET['jsoncallback'] . "(" . json_encode($arr_result) . " ) ";
		break;	

	//Este boton elimina el registro de cuenta corriente y le asigna 0 al status_cuenta_corriente en users
	case 'delete':
			$id_client = (isset($_GET['id_client'])) ? $_GET['id_client'] : '';

			$sql_update = "DELETE FROM cuentas_corrientes
				WHERE id_client = " . $id_client . "";

			$result = deleteDataDB($oConexion, $sql_update);

			//Update a 1 status cuenta corriente en usuarios
			$sql_update_status_cuenta = "UPDATE users
				SET status_cuenta_corriente = '0'
				WHERE id = " . $id_client . "";

			$update_user_status_cuenta = updateDataDB($oConexion, $sql_update_status_cuenta);

			echo $_GET['jsoncallback'] . "(" . json_encode($result) . " ) ";
		break;	
}

?>
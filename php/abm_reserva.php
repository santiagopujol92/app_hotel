<?php
include_once 'includes/conexion.php';
include_once 'includes/funciones.php';

$action = (isset($_GET['action'])) ? $_GET['action'] : '';

switch ($action) {

	case 'insert':

			$id_user = (isset($_GET['id_user'])) ? $_GET['id_user'] : '';
			$name = (isset($_GET['nombre'])) ? $_GET['nombre'] : '';
			$lastname = (isset($_GET['apellido'])) ? $_GET['apellido'] : '';
			$dni = (isset($_GET['dni'])) ? $_GET['dni'] : '';
			$tipo_habitacion = (isset($_GET['tipo_habitacion'])) ? $_GET['tipo_habitacion'] : '';
			$fecha_desde = (isset($_GET['fecha_desde'])) ? $_GET['fecha_desde'] : '';
			$fecha_hasta = (isset($_GET['fecha_hasta'])) ? $_GET['fecha_hasta'] : '';
			$res_number = (isset($_GET['res_number'])) ? $_GET['res_number'] : '';
			$created_at = (isset($_GET['created_at'])) ? $_GET['created_at'] : '';

			$sql = "INSERT INTO bookings (
					id_user, 
					name, 
					lastname, 
					dni, 
					tipo_habitacion, 
					fecha_desde, 
					fecha_hasta, 
					res_number, 
					created_at
				) 
				VALUES (
					'$id_user', 
					'$name', 
					'$lastname', 
					'$dni', 
					'$tipo_habitacion', 
					'$fecha_desde', 
					'$fecha_hasta', 
					'$res_number', 
					'$created_at' 
				)";

			$arr_response = insertDataDB($oConexion, $sql);

			echo $_GET['jsoncallback'] . "(" . json_encode($arr_response) . " ) ";
		break;
	case 'select':
			$id_user = (isset($_GET['id_user'])) ? $_GET['id_user'] : '';
			$dateToday = (isset($_GET['dateToday'])) ? $_GET['dateToday'] : '';

			$filter = false;

			//Si allbookings esta en 'on' al final se reemplaza el sql por todas las reservas desde la fecha
			$allbookings = (isset($_GET['allbookings'])) ? $_GET['allbookings'] : '';

			//Para seleccionar por reserva para editar
			$id_booking = (isset($_GET['id'])) ? $_GET['id'] : 0;
			$res_number = (isset($_GET['res_number'])) ? $_GET['res_number'] : 0;
			if ($id_booking != 0 && $res_number != 0){
				$filter = true;
			}

			//PARA TRAER DATOS DE UNA RESERVA
			if ($filter == true){
				$sql = "SELECT * FROM bookings
					WHERE id = '$id_booking' ";

			//PARA TRAER TODOS LAS RESERVAS DEL USUARIO
			}else{
				$sql = "SELECT b.*, u.preferencia FROM bookings b
							INNER JOIN users u ON u.id = b.id_user 
							WHERE b.id_user = '" . $id_user . "'";

				//Si el filtro es distinto de vacio traer las que el dia de estadia es siguiente o igual al dia de hoy
				if ($dateToday != ''){
					$sql .= " AND b.fecha_desde >= '" . $dateToday . "' ";
				}

				$sql .= " ORDER BY b.fecha_desde DESC ";
							
			}

			if ($allbookings == 'on') {
				// $sql = "SELECT * FROM bookings WHERE fecha_desde >= '" . $dateToday . "' ORDER BY fecha_desde DESC";
				$sql = "SELECT b.*, u.preferencia FROM bookings b 
						INNER JOIN users u ON u.id = b.id_user 
						ORDER BY b.fecha_desde DESC
						";
			}

			$result = getFetchAllDataDB($oConexion, $sql);

			echo $_GET['jsoncallback'] . "(" . json_encode($result) . " ) ";
		break;
	case 'update':
		# code...
		break;
	case 'delete':
		# code...
		break;
}

?>
<?php
include_once 'includes/conexion.php';
include_once 'includes/funciones.php';

$action = (isset($_GET['action'])) ? $_GET['action'] : '';

switch ($action) {

	case 'insert':

			$id_user = (isset($_GET['id_user'])) ? $_GET['id_user'] : '';
			$name = (isset($_GET['nombre'])) ? $_GET['nombre'] : '';
			$lastname = (isset($_GET['apellido'])) ? $_GET['apellido'] : '';
			$dni = (isset($_GET['dni'])) ? $_GET['dni'] : '';
			$tipo_habitacion = (isset($_GET['tipo_habitacion'])) ? $_GET['tipo_habitacion'] : '';
			$fecha_desde = (isset($_GET['fecha_desde'])) ? $_GET['fecha_desde'] : '';
			$fecha_hasta = (isset($_GET['fecha_hasta'])) ? $_GET['fecha_hasta'] : '';
			$res_number = (isset($_GET['res_number'])) ? $_GET['res_number'] : '';
			$created_at = (isset($_GET['created_at'])) ? $_GET['created_at'] : '';

			$sql = "INSERT INTO bookings (
					id_user, 
					name, 
					lastname, 
					dni, 
					tipo_habitacion, 
					fecha_desde, 
					fecha_hasta, 
					res_number, 
					created_at
				) 
				VALUES (
					'$id_user', 
					'$name', 
					'$lastname', 
					'$dni', 
					'$tipo_habitacion', 
					'$fecha_desde', 
					'$fecha_hasta', 
					'$res_number', 
					'$created_at' 
				)";

			$arr_response = insertDataDB($oConexion, $sql);

			echo $_GET['jsoncallback'] . "(" . json_encode($arr_response) . " ) ";
		break;
	case 'select':
			$id_user = (isset($_GET['id_user'])) ? $_GET['id_user'] : '';
			$type_user = (isset($_GET['type_user'])) ? $_GET['type_user'] : '';
			// $dateToday = (isset($_GET['dateToday'])) ? $_GET['dateToday'] : '';

			//Si el parametro id viene entonces devuelve solo un registro de cliente con sus datos
			$where_for_edit = false;
			$id_client = (isset($_GET['id'])) ? $_GET['id'] : 0;
			if ($id_client != 0){
				$where_for_edit = true;
			}
			if ($type_user == 1 || $type_user == 3){

				//PARA TRAER DATOS DE UN CLIENTE
				if ($where_for_edit == true){
					$sql = "SELECT 
							u.id, 
							u.name, 
							u.lastname, 
							u.dni, 
							u.email, 
							u.ciudad, 
							u.pasaporte, 
							u.codigo_postal, 
							u.preferencia,
							c.responsable as responsable_cuenta,
							u.telefono,
							u.mensaje,
							u.type
							FROM users u
							LEFT JOIN cuentas_corrientes c ON c.id_client = u.id 
							WHERE u.id = '$id_client' ";

				//PARA TRAER TODOS LOS CLIENTES DEL USUARIO
				}else{
					//FILTROS
					$filtro_cliente = (isset($_GET['filtro_cliente'])) ? $_GET['filtro_cliente'] : '';

					$sql = "SELECT u.id, 
							u.name, 
							u.lastname, 
							u.dni, 
							u.email, 
							u.ciudad, 
							u.pasaporte, 
							u.codigo_postal, 
							con.descripcion,
							u.preferencia,
							c.responsable as responsable_cuenta,
							u.telefono,
							u.mensaje,
							u.type,
							(SELECT MAX(b.fecha_desde) FROM bookings WHERE b.id_user = id_user LIMIT 1) as ultima_fecha_reserva
							FROM users u 
							LEFT JOIN countries con ON con.id = u.id_country
							LEFT JOIN bookings b ON b.id_user = u.id
							LEFT JOIN cuentas_corrientes c ON c.id_client = u.id 
							WHERE u.status = 'on' ";

					//Si el filtro_cliente viene distinto de vacio lo agrega a la sql.
					if ($filtro_cliente != ''){
						$sql .= " AND (CONCAT(u.lastname, ' ', u.name) LIKE '%".$filtro_cliente."%' ";
						$sql .= " OR CONCAT(u.name, ' ', u.lastname) LIKE '%".$filtro_cliente."%' ";
						$sql .= " OR u.dni LIKE '%".$filtro_cliente."%') ";
					}

					$sql .= " GROUP BY u.id ";
				}

				$result = getFetchAllDataDB($oConexion, $sql);

				echo $_GET['jsoncallback'] . "(" . json_encode($result) . " ) ";
			}
		break;

	case 'update_preferencia':
			$id_client = (isset($_GET['id_client'])) ? $_GET['id_client'] : '';
			$preferencia = (isset($_GET['preferencia'])) ? $_GET['preferencia'] : '';

			$sql = "UPDATE users
			SET preferencia = '" . $preferencia . "'
			WHERE id = " . $id_client . "";

			$result = updateDataDB($oConexion, $sql);
			
			echo $_GET['jsoncallback'] . "(" . json_encode($result) . " ) ";
		break;

	//Modificar en 1 o 2 lo de cuenta corriente o directamente poniendole el id de cuenta corriente en usuarios
	//Buscar en cuentas corrientes si esta el id de cliente en algun lado, si lo encunetra y se esta cargando una cuenta
	//corriente se inserta una nueva con el id
	//Si encuentra el cliente en una cuenta corriente se hace update de esa cuenta corriente.
	case 'create_update_cuenta_corriente':

			$update_user_status_cuenta = updateDataDB($oConexion, $sql);

			//Insertar si el id cliente no existe en una cuenta y se esta asignando una
			$insert_cuenta = insertDataDB($oConexion, $sql);

			//Updatear si id cliente existe en una cuenta
			$update_cuenta = updateDataDB($oConexion, $sql);

			echo $_GET['jsoncallback'] . "(" . json_encode($result) . " ) ";
		break;	

	case 'delete_cuenta_corriente':

			//Hacer un boton que haga el delete de la cuenta corriente y borre el registro y pona en 0 al usuario.
			
			echo $_GET['jsoncallback'] . "(" . json_encode($result) . " ) ";
		break;	

	case 'update_mensaje':
			$id_client = (isset($_GET['id_client'])) ? $_GET['id_client'] : '';
			$mensaje = (isset($_GET['mensaje'])) ? $_GET['mensaje'] : '';

			$sql = "UPDATE users
			SET mensaje = '" . $mensaje . "'
			WHERE id = " . $id_client . "";

			$result = updateDataDB($oConexion, $sql);
			
			echo $_GET['jsoncallback'] . "(" . json_encode($result) . " ) ";
		break;

	case 'update_type_user':
			$id_client = (isset($_GET['id_client'])) ? $_GET['id_client'] : '';
			$id_type_user = (isset($_GET['id_type_user'])) ? $_GET['id_type_user'] : '';
			$type_user_logged = (isset($_GET['type_user_logged'])) ? $_GET['type_user_logged'] : '';
			
			if ($type_user_logged == 3){
				$sql = "UPDATE users
				SET type = '" . $id_type_user . "'
				WHERE id = " . $id_client . "";

				$result = updateDataDB($oConexion, $sql);
				echo $_GET['jsoncallback'] . "(" . json_encode($result) . " ) ";
			}
			
		break;
}

?>
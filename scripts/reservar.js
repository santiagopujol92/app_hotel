
//VERIFICAR HABITACIONES PARA RESERVA Y SI SE PUEDE LLAMAR FUNCION PARA REGISTRAR
function verifyAndRegisterReserve(){
	//LOS TIPOS DE HABITACION HARDCODEAR EL VALOR Y ASIGNARLO EN LA PARTE DE ROMTYPE,
	//FROM Y TO DE LAS FECHAS
	//USUARIO Y HOTEL VA HARDCODEADO Q ES EL USUARIO Q ACCEDE A MINI HOTEL Y ID HOTEL EL NOMBRE DEL HOTEL EN SETTING
	//EN PRICES PONER UNO POR DEFECTO
	//PARA Q CUANDO SE MANDE EL XML YA VAYA CONFIGURADO DE ACUERDO AL FORM

	var url = "http://api.minihotelpms.com/GDS";

	var tipo_habitacion = $("#formReserva select[name=tipo_habitacion]").val();
	var fecha_desde = $("#formReserva input[name=fecha_desde]").val();
	var fecha_hasta = $("#formReserva input[name=fecha_hasta]").val();
	// var cant_adultos = $("#formReserva select[name=adultos]").val();
	// var cant_ninios = $("#formReserva select[name=ninios]").val();
	// var cant_bebes = $("#formReserva select[name=bebes]").val();

    //Validar formulario
    var array_checkForm = checkFormShowingMsg('formReserva');
    var msg = '';
    if (array_checkForm['puedeGuardar'] == false){
        msg = array_checkForm['msg'];
        showMessage('messageReserva', 'alert-warning', msg);
        // showMessageModal(msg);
        return false;
    }

	//XML DE VERIFICACION
	var xml_verify = '' +
		'<?xml version="1.0" encoding="UTF-8" ?>' +

		'<AvailRaterq xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">'+

			'<Authentication username="TestTours" password="2222" />'+
			// '<Authentication username="GivenUser" password="GivenPassword"/>'+
			'<Hotel id="testhotel" Currency="ILS"/>'+

			'<DateRange from="'+fecha_desde+'" to="'+fecha_hasta+'" />'+
			'<Guests adults="1" child="0" babies="0" />'+
			'<RoomTypes>'+
				'<RoomType id="'+tipo_habitacion+'" />'+
			'</RoomTypes>'+

			'<Prices rateCode="*ALL">'+
				'<Price boardCode="RO" />'+
				'<Price boardCode="BF" />'+
				'<Price boardCode="HBL"/>'+
				'<Price boardCode="HBD"/>'+
			'</Prices>'+

		'</AvailRaterq>';

	//LLAMADA A WEB SERVICE PARA VERIFICAR
	$.support.cors = true;
	$.ajax({
	    url: url,
	    type: 'POST',
	    crossDomain: true,
	    data: xml_verify,
	    dataType: 'text',
	    username: 'TestTours',
	    password: '2222',
        beforeSend: function () {
            $("#btnregistrarReserva").html("<img width='25' src='images/Rolling.gif'></img> Verificando ..");
        },
	    success: function (response) {
	        var error_code = response.substring(4, 7);
	        var error_msg = response.substring(9, 999);

	        //LEER LA RESPUESTA Y BUSCAR CODIGOS DE ERROR SI SE ENCUENTRAN MOSTRARLOS
	        //SINO SE ENCUENTRAN LLAMAR REGISTRO RESERVA PASANDO URL
	        //CADA ERROR REPRESENTA UN MENSAJE
			msg_error_formatted = formatMsgWithErrorCodeAndMsg(error_code, error_msg);

			//SI DEVUELVE XML BUSCAMOS LOS ROOMS PARA MOSTRAR VALOR
			if (msg_error_formatted == false){
				
				//Pasar String XML a Objeto XML
				var xmlDoc = parsearXml(response);

				//Leer Objeto XML con respuesta web service
				if (xmlDoc) {
					//Si no encuentra roomtype en el xml significa que no hay habitaciones disponibles para ese rango de fecha y habitacion
					if (typeof(xmlDoc.getElementsByTagName("RoomType")[0]) != 'undefined'){
						//Nombre
						var roomType = xmlDoc.getElementsByTagName("RoomType")[0];
						var attr_Name_e = roomType.getAttributeNode("Name_e");
						var value_name_e = attr_Name_e.nodeValue;

						//Valor por dia
						var roomType_inventory = xmlDoc.getElementsByTagName("Inventory")[0];
						var attr_maxavail = roomType_inventory.getAttributeNode("maxavail");
						var value_max_avail = attr_maxavail.nodeValue;

						//Habitaciones Disponibles
						var roomType_price = xmlDoc.getElementsByTagName("price")[0];
						var attr_price_x_night = roomType_price.getAttributeNode("value");
						var value_price_x_night = attr_price_x_night.nodeValue;

						if (confirm(value_max_avail + ' Habitaciones Disponibles \n \n'+
							'¿Confirmar Reserva de Habitación?\n\n'+
							'Datos: \n'+
							'-Tipo Habitación: '+value_name_e+' \n'+
							'-Reservación desde '+formatDateTime(fecha_desde, 'd-m-Y')+' Hasta '+formatDateTime(fecha_hasta, 'd-m-Y')+' \n'+
							'-Precio por noche: $'+ value_price_x_night)){
							
							showMessage('messageReserva', 'alert-success', 'Hay disponibilidad para realizar la reserva .. Reservando <img width="30" src="images/Rolling.gif"></img>');
				    		registrarReserva(url);
						}else{
							$("#btnregistrarReserva").html("Registrar");
							return false;
						}

					}else{
						showMessage('messageReserva', 'alert-danger', 'No hay habitaciones disponible en ese rango de fecha y tipo de habitación');
						$("#btnregistrarReserva").html("Registrar");
					}
				}

			//Si encuentra roomtype ahi si registra sino no hay habitaicones disponibles
			}else{
				showMessage('messageReserva', 'alert-danger', msg_error_formatted);
				$("#btnregistrarReserva").html("Registrar");
			}
	    },
	    error: function (jqXHR, tranStatus, errorThrown) {
	        alert(
	            'Status: ' + jqXHR.status + ' ' + jqXHR.statusText + '. ' +
	            'Response: ' + jqXHR.responseText
	        );
	        $("#btnregistrarReserva").html("Registrar");
	    }
	});
}

function registrarReserva(url){
	var nombre = $("#formReserva input[name=name]").val();
	var apellido = $("#formReserva input[name=lastname]").val();
	var dni = $("#formReserva input[name=dni]").val();
	var tipo_habitacion = $("#formReserva select[name=tipo_habitacion]").val();
	var fecha_desde = $("#formReserva input[name=fecha_desde]").val();
	var fecha_hasta = $("#formReserva input[name=fecha_hasta]").val();

	// var cant_adultos = $("#formReserva select[name=adultos]").val();
	// var cant_ninios = $("#formReserva select[name=ninios]").val();
	// var cant_bebes = $("#formReserva select[name=bebes]").val();

	//Dia de hoy formateado
	var date = new Date();
	var dateDB = date.getFullYear() +'-'+ (parseFloat(date.getMonth()) + 1) + '-' + date.getDate();
	var dateToday = formatDateTime(dateDB, 'd-m-Y');

	//CREAR RESERVA
	//CAMBIAR ID CADA VEZ QUE SE CREA UNA RESERVA
	var xml_register = '' +
		'<?xml version="1.0" encoding="UTF-8" ?>' +

		'<Bookings>'+
			'<Authentication username="TestTours" password="2222" />'+
			'<Hotel id="testhotel" />'+

			'<Booking type="Book" createDateTime="'+dateToday+'" source="Booking Sent This">'+
				'<RoomStay roomTypeID="Suite" ratePlanID="HB" roomTypeName="Suite">'+
					'<StayDate arrival="'+fecha_desde+'" departure="'+fecha_hasta+'" />'+
					'<RoomCount NumberOfUnits="1" />'+

					'<GuestCount adult="1" child="0" baby="0" />'+

					'<PerDayRates CurrencyCode=" ILS ">'+
						'<PerDayRate stayDate="" baseRate="" />'+
						'<PerDayRate stayDate="" baseRate="" />'+
						'<PerDayRate stayDate="" baseRate="" />'+
					'</PerDayRates>'+

					'<Total AmountAfterTaxes="1910.00" CurrencyCode="ILS" />'+

					'<GuestNames>'+
						'<Name givenName="Barack" surname="Obama" />'+
					'</GuestNames>'+
				'</RoomStay>'+

				'<PrimaryGuest>'+
					'<Name givenName="'+nombre+'" surname="'+apellido+'" />'+
					'<Address Street="" Zip="" City="" />'+
					'<Country CountryName="" iso2="" iso3="" />'+
					'<Language iso2="" />'+
					'<Email>support@motel-soft.com</Email>'+
					'<Phone>077-2034564</Phone>'+
					'<Fax></Fax>'+
					'<CreditCard Type="" Number="" NameOnCard="" Expirationdate="" />'+
				'</PrimaryGuest>'+

				'<Remarks>Name Agent : Agent Test</Remarks>'+

				'<ResGlobalInfo>'+
					'<Timespan arrival="" departure="" />'+
					'<Total AmountAfterTaxes="" CurrencyCode="ILS" />'+
				'</ResGlobalInfo>'+
			'</Booking>'+
		'</Bookings>';


// SOLO SE PUEDE ACCEDER AL HOTEL demo2 para el mapa, PERO NO SE PUEDE REGISTRAR EN ESE HOTEL
 
		// '<?xml version="1.0" encoding="UTF-8" ?>'+
		// '<Bookings>'+
		// 	'<Authentication username="demo" password="2222"/>'+
		// 	'<Hotel id="testhotel" Currency="USD"/>'+
		// 	'<Booking type="Book" createDateTime="'+dateToday+'" source="testsource">'+
		// 		'<RoomStay roomTypeID="APT1" ratePlanID="" roomTypeName="">'+
		// 			'<StayDate arrival="'+fecha_desde+'" departure="'+fecha_hasta+'"/>'+
		// 			'<RoomCount NumberOfUnits="1"/>'+
		// 			'<GuestCount adult="2" child="0" baby="0"/>'+
		// 			'<Total AmountAfterTaxes="2600" CurrencyCode="nis"/>'+
		// 			'<GuestNames>'+
		// 				'<Name givenName="test" surname="test"/>'+
		// 			'</GuestNames>'+
		// 		'</RoomStay>'+
				 
		// 		'<PrimaryGuest>'+
		// 			'<Name givenName="'+nombre+'" surname="'+apellido+'" />'+
		// 			'<Address Street="" Zip="" City="" />'+
		// 			'<Country CountryName="" iso2="" iso3="" />'+
		// 			'<Language iso2="" />'+
		// 			'<Email>support@motel-soft.com</Email>'+
		// 			'<Phone>077-2034564</Phone>'+
		// 			'<Fax></Fax>'+
		// 			'<CreditCard Type="" Number="" NameOnCard="" Expirationdate="" />'+
		// 		'</PrimaryGuest>'+

		// 		'<Remarks>Made By Anatoly</Remarks>'+
		// 	'</Booking>'+
		// '</Bookings>';


// '<?xml version="1.0" encoding="UTF-8"?>'+
// '<AvailRaters>'+
// '<Authentication username="TestTours" password="2222"/>'+
// '<Hotel id="testhotel" Name_h="Motel Demo1" Name_e="Motel Demo1"'+
// 'Currency=""/>'+
// '<DateRange from="2015-02-20" to="2015-02-22"/>'+
// '<Guests adults="2" child="0" babies="0"/>'+
// '<RoomType id="dbp" Name_h="double private" Name_e="double private">'+
// '<Inventory Allocation="4" maxavail="4"/>'+
// '<price board="B5" boardDesc="Breakfast+N+S/AA" value="1788"/>'+
// '<price board="BB" boardDesc="Breakfast" value="1220"/>'+
// '<price board="HB" boardDesc="Half Board" value="1500"/>'+
// '<price board="HBD" boardDesc="breakfast and dinner" value="1500"/>'+
// '<price board="HBL" boardDesc="Half board L" value="1508"/>'+
// '<price board="RO" boardDesc="Room only" value="1220"/>'+
// '</RoomType>'+
// '</AvailRaters>';




	//LLAMADA A WEB SERVICE PARA VERIFICAR
	$.support.cors = true;
	$.ajax({
	    url: url,
	    type: 'POST',
	    crossDomain: true,
	    data: xml_register,
	    dataType: 'text',
	    username: 'TestTours',
	    password: '2222',
        beforeSend: function () {
            $("#btnregistrarReserva").html("<img width='25' src='images/Rolling.gif'></img> Registrando ..");
        },
	    success: function (response) {
	        var error_code = response.substring(4, 7);
	        var error_msg = response.substring(9, 999);
			msg_error_formatted = formatMsgWithErrorCodeAndMsg(error_code, error_msg);

			if (msg_error_formatted == false){

				//Pasar String XML a Objeto XML
				var xmlDoc = parsearXml(response);

				//Leer Objeto XML con respuesta web service
				if (xmlDoc) {
					//Data booking
					//idReserva
					if (typeof(xmlDoc.getElementsByTagName("BookingConfirmNumber")[0]) != 'undefined'){

						var dataBooking = xmlDoc.getElementsByTagName("BookingConfirmNumber")[0];
						var attr_booking_id = dataBooking.getAttributeNode("bookingID");
						var id_booking = attr_booking_id.nodeValue;

						//res number
						var attr_resnumber = dataBooking.getAttributeNode("resnumber");
						var res_number = attr_resnumber.nodeValue;

						//Guardar Esta Reserva en BD App
						array_reserve = new Array();
						array_reserve['id_user'] = localStorage.getItem('id_user');
						array_reserve['nombre'] = nombre;
						array_reserve['apellido'] = apellido;
						array_reserve['dni'] = dni;
						array_reserve['tipo_habitacion'] = tipo_habitacion;
						array_reserve['fecha_desde'] = fecha_desde;
						array_reserve['fecha_hasta'] = fecha_hasta;
						array_reserve['resnumber'] = res_number;
						array_reserve['created_at'] = dateDB;
						guardarReservaEnBD(array_reserve);

						$("#btnregistrarReserva").html("Registrar");

						showMessage('messageReserva', 'alert-success', 'Reserva Realizada Con Éxito');
						showMessageModal('Reserva Realizada Con Éxito', 'Felicidades !', 'text-success', 'success');
					}else{
						showMessage('messageReserva', 'alert-danger', 'No se pudo registrar la reserva. Intente nuevamente');
						showMessageModal('No se pudo registrar la reserva. Intente nuevamente', 'Error !', 'text-danger', 'remove');
						$("#btnregistrarReserva").html("Registrar");
					}

				}else{
					showMessage('messageReserva', 'alert-danger', msg_error_formatted);
					$("#btnregistrarReserva").html("Registrar");
				}
			}
	    },
	    error: function (jqXHR, tranStatus, errorThrown) {
	        alert(
	            'Status: ' + jqXHR.status + ' ' + jqXHR.statusText + '. ' +
	            'Response: ' + jqXHR.responseText
	        );
	        $("#btnregistrarReserva").html("Registrar");
	    }
	});

}

//Registra reserva en BD APP
function guardarReservaEnBD(array_reserve){
	var url = root_project + "php/abm_reserva.php?action=insert&jsoncallback=?";

	//Llamada backend
    $.getJSON(url,
        {
			'id_user' : array_reserve['id_user'],
			'nombre' : array_reserve['nombre'],
			'apellido' : array_reserve['apellido'],
			'dni' : array_reserve['dni'],
			'tipo_habitacion' : array_reserve['tipo_habitacion'],
			'fecha_desde' : array_reserve['fecha_desde'],
			'fecha_hasta' : array_reserve['fecha_hasta'],
			'res_number' : array_reserve['resnumber'],
			'created_at' : array_reserve['created_at']
        })
    .done(function(respuesta) { 
        console.log('Peticion Web Service EXITOSA');

         if (respuesta['code_error'] == 1062){
                if (respuesta['msg'].indexOf("for key 'res_number'") > 0){
                    console.log('No se pudo registra la reserva en la base de datos de la app. Ya existe una reserva con ese NÚMERO');
                    return false;
                }
            }else if (respuesta['code_error'] == 0){  
                console.log('Reserva registrada con éxito');
            }
    }).fail(function( jqxhr, textStatus, error ) {
        console.log(
            'Status: ' + jqxhr + ' ' + textStatus + '. ' +
            'Response: ' + error
        );
    });
}

jQuery(document).ready(function($) {
	getCuentasCorrientes();
});

function getCuentasCorrientes(){
    var url = root_project + "php/abm_cuenta_corriente.php?action=select&jsoncallback=?";
 	var id_user = localStorage.getItem('id_user');
    var type_user = localStorage.getItem('type');

    var filtro_cuenta = $("#formBuscarCuenta input[name=filtro_cuenta]").val();

    $.getJSON(url,
        {
            "id_user":id_user,
            "type_user":type_user,
            "filtro_cuenta":filtro_cuenta
        })
    .done(function(data) { 
        $("#tbody_cuentas").empty();
        statusLoading('loadingCuentas', 1 , 'Cargando ..');
        if (data){
        	$.each(data, function(index, res) {
                if (res.responsable == null){
                    res.responsable = '';
                }

                if (res.tipo_cuenta == 'A'){
                    res.tipo_cuenta = 'Factura A';
                }else if (res.tipo_cuenta == 'B'){
                    res.tipo_cuenta = 'Factura B';
                }else if (res.tipo_cuenta == 'C'){
                    res.tipo_cuenta = 'Cuenta Corriente';
                }

        		$("#tbody_cuentas").append(
            		'<tr class="hovered">'+
                        '<td>'+res.name+' '+res.lastname+'</td>'+
						'<td>'+res.responsable+'</td>'+
						'<td>'+res.empresa+'</td>'+
                        '<td>'+res.tipo_cuenta+'</td>'+
						'<td>'+
							'<a type="button" title="Editar Cuenta Corriente" class="btn btn-primary" data-toggle="modal" data-target="#modalCuentaCorriente" onclick="abrirModalCuentaCorriente('+res.id_client+')" data-whatever="@mdo">'+
								'<i class="	glyphicon glyphicon-edit" aria-hidden="true"></i>'+
							'</a>&nbsp;'+
                            '<a type="button" title="Eliminar Cuenta Corriente" class="btn btn-danger" data-toggle="modal" data-target="#modalEliminarCuentaCorriente" onclick="configButtonEliminarCuentaCorriente('+res.id_client+')" data-whatever="@mdo">'+
                                '<i class=" glyphicon glyphicon-remove" aria-hidden="true"></i>'+
                            '</a>'+
						'</td>'+
					'</tr>'	
				);
        	});
            
            statusLoading('loadingCuentas', 0);
            $("#total_resultado").html(data.length);
        }else{  
        	showMessage('messageCuentasList', 'alert-warning', "No hay cuentas corrientes");
            statusLoading('loadingCuentas', 0);
        }
    }).fail(function( jqxhr, textStatus, error ) {
        showMessage('messageCuentasList', 'alert-danger', "Error al obtener los cuentas corrientes o acceso denegado");
        statusLoading('loadingCuentas', 0);
    });
}


function abrirModalCuentaCorriente(id){

    var url = root_project + "php/abm_cuenta_corriente.php?action=select&jsoncallback=?";
    statusLoading('loadingmodalCuentaCorriente', 1 , 'Cargando ..');
    var type_user = localStorage.getItem('type');

    clearForm('formCuentaCorriente');
    $("#formCuentaCorriente input[name=status_cuenta_corriente]").val('0');
    $("#modalCuentaCorrienteTitle").html('Cuenta Corriente');

    $.getJSON(url,
        {
            "id":id,
            "type_user":type_user,
        })
    .done(function(data) { 

        $("#formCuentaCorriente #btnGuardarCuentaCorriente").attr('onclick', 'guardarCuentaCorriente('+id+')');
        $("#formCuentaCorriente #btnEliminarCuentaCorriente").attr('onclick', 'eliminarCuentaCorriente('+id+')');

        if (data){
            $.each(data, function(index, res) {
                $("#modalCuentaCorrienteTitle").html('Cuenta Corriente - <b>' + res.name + " " + res.lastname + '</b>');

                $("#formCuentaCorriente input[name=responsable]").val(res.responsable);
                $("#formCuentaCorriente input[name=empresa]").val(res.empresa);
                $("#formCuentaCorriente select[name=tipo_cuenta]").val(res.tipo_cuenta);
                $("#formCuentaCorriente input[name=status_cuenta_corriente]").val(res.status_cuenta_corriente);

                statusLoading('loadingmodalCuentaCorriente', 0);
            });
        }else{
            showMessage('msgModalCuentaCorriente', 'alert-info', "Este cliente no tiene cuenta corriente asignada");
            statusLoading('loadingmodalCuentaCorriente', 0);
        }
    }).fail(function( jqxhr, textStatus, error ) {
        showMessage('msgModalCuentaCorriente', 'alert-danger', "Error al obtener datos de la cuenta");
        statusLoading('loadingmodalCuentaCorriente', 0);
    });
}

function guardarCuentaCorriente(id){
    var url = root_project + "php/abm_cuenta_corriente.php?action=insert_update_cuenta_corriente&jsoncallback=?";

    var responsable = $("#formCuentaCorriente input[name=responsable]").val();
    var empresa = $("#formCuentaCorriente input[name=empresa]").val();
    var tipo_cuenta = $("#formCuentaCorriente select[name=tipo_cuenta]").val();
    var status_cuenta_corriente = $("#formCuentaCorriente input[name=status_cuenta_corriente]").val();

    array_check = checkFormShowingMsg('formCuentaCorriente');
    if (array_check['puedeGuardar']){
        $("#btnGuardarCuentaCorriente").html('<img width="15px" style="margin-bottom:5px;" src="images/Rolling.gif"/> Guardando');
        $.getJSON(url,
            {
                'id_client' : id,
                'responsable' : responsable,
                'empresa' : empresa,
                'tipo_cuenta' : tipo_cuenta,
                'status_cuenta_corriente' : status_cuenta_corriente
            })
        .done(function(respuesta) { 
            showMessageModal('Cuenta corriente actualizada con éxito', 'Bien !', 'text-success', 'success');
            $("#btnGuardarCuentaCorriente").html('Guardar');
            $("#modalCuentaCorriente").modal('hide');
            getCuentasCorrientes();
        }).fail(function( jqxhr, textStatus, error ) {
            console.log(
                'Status: ' + jqxhr + ' ' + textStatus + '. ' +
                'Response: ' + error
            );
            $("#btnGuardarCuentaCorriente").html('Guardar');
            showMessageModal('Error al guardar la cuenta corriente', 'Error !', 'text-danger', 'danger');
        });
    }else{
        showMessageModal(array_check['msg']);
        return false;
    }

}

function configButtonEliminarCuentaCorriente(id){
    $("#btnEliminarCuentaCorriente").attr('onclick', 'eliminarCuentaCorriente('+id+')')
}

function eliminarCuentaCorriente(id){
    var url = root_project + "php/abm_cuenta_corriente.php?action=delete&jsoncallback=?";

    $("#btnEliminarCuentaCorriente").html('<img width="15px" style="margin-bottom:5px;" src="images/Rolling.gif"/> Eliminando');
    $.getJSON(url,
        {
            'id_client' : id,
        })
    .done(function(respuesta) { 
        showMessageModal('Cuenta corriente eliminada con éxito', 'Bien !', 'text-success', 'success');
        $("#btnEliminarCuentaCorriente").html('Eliminar');
        $("#modalEliminarCuentaCorriente").modal('hide');
        getCuentasCorrientes();
    }).fail(function( jqxhr, textStatus, error ) {
        console.log(
            'Status: ' + jqxhr + ' ' + textStatus + '. ' +
            'Response: ' + error
        );
        $("#modalEliminarCuentaCorriente").modal('hide');
        showMessageModal('Error al eliminar la cuenta corriente', 'Error !', 'text-danger', 'danger');
    });

}
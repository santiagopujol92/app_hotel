function validarLogin(){
    //Validar formulario
    var array_checkForm = checkFormShowingMsg('formLogin');
    var msg = '';
    if (array_checkForm['puedeGuardar'] == false){
        msg = array_checkForm['msg'];
        showMessage('message_login', 'bg-warning', msg, 'warning');
        // showMessageModal(msg);
        return false;
    }

    //Variables
    var usuario = $("#formLogin input[name=user]").val();
    var password = $("#formLogin input[name=password]").val();
    // var type = $("#formLogin input[name=type_user]").val();
    var url = root_project + "php/login.php?jsoncallback=?";

    $.getJSON(url,
        {
            "user":usuario,
            "pass":password,
            // "type_user":type
        })
        .done(function(respuesta) { 
            if (respuesta == 0){
                showMessage('message_login', 'bg-danger', "Usuario o Contraseña incorrecta", 'remove');
                localStorage.clear();
            }else{  
                localStorage.clear();
                localStorage.setItem("id_user", respuesta['id']);
                localStorage.setItem("user", respuesta['user']);
                localStorage.setItem("type", respuesta['type']);
                localStorage.setItem("status", respuesta['status']);
                localStorage.setItem("name", respuesta['name']);
                localStorage.setItem("lastname", respuesta['lastname']);
                localStorage.setItem("dni", respuesta['dni']);
                localStorage.setItem("email", respuesta['email']);
                localStorage.setItem("mensaje", respuesta['mensaje']);
                
                window.location.href = 'principal.html';
            }
        }).fail(function( jqxhr, textStatus, error ) {
            showMessage('message_login', 'bg-danger', "Usuario o Contraseña incorrecta", 'remove');
        });
} 

//Registrar usuario
function registrarUsuario(){
    //Validar formulario
    var array_checkForm = checkFormShowingMsg('formRegisterUser');
    var msg = '';
    if (array_checkForm['puedeGuardar'] == false){
        msg = array_checkForm['msg'];
        showMessage('message_register', 'bg-warning', msg, 'warning');
        return false;
    }
    //VALIDAR CONTRASEÑAS
    if ($("#formRegisterUser input[name=confirm]").val() == $("#formRegisterUser input[name=password]").val()){

        //Validar email
        if (!isEmail($("#formRegisterUser input[name=email]").val())){
            showMessage('message_register', 'bg-warning','<strong>Atención ! </strong><br> El email ingresado no es valido', 'warning');
            setBorderInput($("#formRegisterUser input[name=email]"), '#ffc107');
            return false;
        }

        //Variables
        var name = $("#formRegisterUser input[name=name]").val();
        var lastname = $("#formRegisterUser input[name=lastname]").val();
        var user = $("#formRegisterUser input[name=user]").val();
        var password = $("#formRegisterUser input[name=password]").val();
        var email = $("#formRegisterUser input[name=email]").val();
        var type_user = $("#formRegisterUser input[name=type_user]").val();
        var dni = $("#formRegisterUser input[name=dni]").val();
        var telefono = $("#formRegisterUser input[name=telefono]").val();
        var url = root_project + "php/register_user.php?jsoncallback=?";

        //Llamada backend
        $.getJSON(url,
            {
                "name":name,
                "lastname":lastname,
                "dni":dni,
                "email":email,
                "user":user,
                "pass":password,
                "type":type_user,
                "telefono":telefono
            })
        .done(function(respuesta) { 

            checkFormShowingMsg('formRegisterUser');

            //ACA VALIDAR EMAIL Y USUARIO DISPONIBLE
            if (respuesta['code_error'] == 1062){
                if (respuesta['msg'].indexOf("for key 'users_email_unique'") > 0){
                    showMessage('message_register', 'bg-danger', "<strong>Error ! </strong><br> Ya existe un usuario con el email ingresado. ", 'remove');
                    showMessageModal("Ya existe un usuario con el email ingresado. ", 'Atención !', 'warning');
                    setBorderInput($("#formRegisterUser input[name=email]"), '#dc3545');
                }
                if (respuesta['msg'].indexOf("for key 'user'") > 0){
                    showMessage('message_register', 'bg-danger', "<strong>Error ! </strong><br> Ya existe un usuario con el nombre de usuario ingresado. ", 'remove');
                    showMessageModal("Ya existe un usuario con el nombre de usuario ingresado. ", 'Atención !', 'warning');
                    setBorderInput($("#formRegisterUser input[name=user]"), '#dc3545');
                }
            }else if (respuesta['code_error'] == 0){  
                showMessage('message_register', 'bg-success', "'<strong>Felicidades ! </strong> Usuario Registrado con Éxito.", 'success');
                
                //Una vez registrado se guardan en sesion los datos
                localStorage.clear();
                localStorage.setItem("id_user", respuesta['id']);
                localStorage.setItem("user", user);
                localStorage.setItem("type", type_user);
                localStorage.setItem("status", 'on');
                localStorage.setItem("name", name);
                localStorage.setItem("lastname", lastname);
                localStorage.setItem("dni", dni);
                localStorage.setItem("email", email);

                //Una vez registrado sale un mensaje para confirmar iniciar sesion. si cancela se borran los datos de sesion
                showMessageModal("Bienvenido "+name+", <br>Usuario Registrado con Éxito. <br>Presione Ok para iniciar sesion o Cancelar para volver al inicio.", 'Felicidades !', 'text-success', 'success', "window.location.href='index.html';", 'localStorage.clear();');
                clearForm('formRegisterUser', true);

            }
        }).fail(function( jqxhr, textStatus, error ) {
            checkFormShowingMsg('formRegisterUser');
            showMessageModal("Error de Conexión. Intente nuevamente. ", 'ERROR !', 'danger');
            showMessage('message_register', 'bg-danger', "'<strong>Error ! </strong> Error de Conexión. Intente nuevamente. ", 'remove');
        });

    } else{
        showMessageModal('Las Contraseñas deben ser iguales. ', 'Atención !', 'warning');
        showMessage('message_register', 'bg-warning','<strong>Atención! </strong><br>Las Contraseñas deben ser iguales. ', 'warning'); 
        setBorderInput($("#formRegisterUser input[name=confirm]"), '#ffc107');
    }

}
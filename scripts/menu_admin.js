//Configuracion de menu para cada usuario
$(document).ready(function() {			
	if (localStorage.getItem('type') == 1 || localStorage.getItem('type') == 3){
		$("#menu-admin-clientes").html('<a href="clientes.html" data-hover="Clientes">'+
							'<i class="glyphicon glyphicon-user" aria-hidden="true"></i> '+
							'Clientes'+
							'</a>');

		$("#menu-admin-cuentas").html('<a href="cuentas_corrientes.html" data-hover="Cuentas Corrientes">'+
							'<i class="glyphicon glyphicon-briefcase" aria-hidden="true"></i> '+
							'Cuentas Corrientes'+
						'</a>');
	}
	if (localStorage.getItem('type') == 1){
		$("#menu-reservar").html('');

		$("#menu-reservar_otra").html('');

		$("#menu-reservar2").html('');

		$("#menu-reservar3").html('');
	}
});

var id_user = localStorage.getItem('id_user');
var user = localStorage.getItem('user');
var type_user = localStorage.getItem('type');
var status_user = localStorage.getItem('status');
var name_user = localStorage.getItem("name");
var lastname_user = localStorage.getItem("lastname");
var dni_user = localStorage.getItem("dni");
var email_user = localStorage.getItem("email");

if (localStorage.getItem("mensaje") != null){
	var mensaje = localStorage.getItem("mensaje");
}else{
	var mensaje = '';
}

if (id_user == null || email_user == null || status_user == 'off' || status_user == 0 || status_user == null ){
  	window.location.href = 'index.html';
}

function cerrarSesion(){
    localStorage.clear();
    window.location.href = 'index.html';
}

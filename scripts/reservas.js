jQuery(document).ready(function($) {
	getReservasUser();
});

function getReservasUser(){
    var id_user = localStorage.getItem('id_user');
    var type_user = localStorage.getItem('type');
    var url = root_project + "php/abm_reserva.php?action=select&jsoncallback=?";

    //Si el tipo de usuario es Admin trae todas las reservas q hay a partir de la fecha
    if (type_user == 1 || type_user == 3){
        url = root_project + "php/abm_reserva.php?action=select&allbookings=on&jsoncallback=?";
    }

 	//Dia de hoy formateado
	var date = new Date();
	var dateDB = date.getFullYear() +'-'+ (parseFloat(date.getMonth()) + 1) + '-' + date.getDate();

    $.getJSON(url,
        {
            "id_user":id_user,
            "dateToday":dateDB
        })
    .done(function(data) { 
        if (data){
        	$.each(data, function(index, res) {
            	var tipo_habitacion = formatShowTipoHabitacion(res.tipo_habitacion);

        		$("#tbody_reservas").append(
            		'<tr class="hovered" onclick="abrirModalReservaEdit('+res.id+', '+res.res_number+');" data-toggle="modal" data-target="#modalEditarReserva">'+
						'<th scope="row">'+res.res_number+'</th>'+
                        '<td>'+name_user+' '+lastname_user+'</td>'+
                        '<td>'+res.preferencia+'</td>'+
						'<td>'+res.name+' '+res.lastname+'</td>'+
						'<td>'+res.dni+'</td>'+
						'<td>'+tipo_habitacion+'</td>'+
						'<td>'+formatDateTime(res.fecha_desde, 'd-m-Y')+'</td>'+
						'<td>'+formatDateTime(res.fecha_hasta, 'd-m-Y')+'</td>'+
						'<td>'+
							// '<a type="button" title="Editar Reserva" class="btn btn-primary" data-toggle="modal" data-target="#modalEditarReserva" onclick="abrirModalReservaEdit('+res.id+', '+res.res_number+')" data-whatever="@mdo">'+
							// 	'<i class="	glyphicon glyphicon-edit" aria-hidden="true"></i>'+
							// '</a>'+
                            '<a type="button" title="Ver Reserva" class="btn btn-success"  onclick="abrirModalReservaEdit('+res.id+', '+res.res_number+')" data-whatever="@mdo">'+
                                '<i class=" glyphicon glyphicon-search" aria-hidden="true"></i>'+
                            '</a>'+
							// '<a type="button" title="Eliminar Reserva" data-toggle="modal" style="margin-left:4px;" data-target="#modalCancelarReserva" class="btn btn-danger" onclick="abrirModalReservaDelete('+res.id+', '+res.res_number+')" data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo">'+
							// 	'<i class="	glyphicon glyphicon-remove" aria-hidden="true"></i>'+
							// '</a>'+
                            '<a type="button" data-toggle="modal" title="Enviar email a responsable" href="mailto:hotelelvi3rrey@example.com?subject=Baja de Reserva NRO: '+res.res_number+'&message=Solicito dar de baja a la reserva número '+res.res_number+'" style="margin-left:4px;" data-target="" class="btn btn-warning" data-whatever="@mdo">'+
                             '<i class="glyphicon glyphicon-envelope" aria-hidden="true"></i>'+
                            '</a>'+
						'</td>'+
					'</tr>'	
				);
        	});
            
            showMessage('messageReservasList', 'alert-success', "Reservas actualizadas con éxito");
            $("#total_resultado").html(data.length);

        }else{  
        	showMessage('messageReservasList', 'alert-warning', "No hay reservas solicitadas");
        }
    }).fail(function( jqxhr, textStatus, error ) {
        showMessage('messageReservasList', 'alert-danger', "Error al obtener las reservas");
    });
}

//Id para la base local dela app, y res_number para la base del hotel(xml send)
//Levantar Reserva
function abrirModalReservaEdit(id, res_number){

    var url = root_project + "php/abm_reserva.php?action=select&jsoncallback=?";
    statusLoading('loadingModalReserva', 1 , 'Cargando ..');
    $.getJSON(url,
        {
            "id":id,
            "res_number":res_number
        })
    .done(function(data) { 
        if (data){
            $.each(data, function(index, res) {

                $("#formEditarReserva input[name=name]").val(res.name);
                $("#formEditarReserva input[name=lastname]").val(res.lastname);
                $("#formEditarReserva input[name=dni]").val(res.dni);
                $("#formEditarReserva input[name=fecha_desde]").val(res.fecha_desde);
                $("#formEditarReserva input[name=fecha_hasta]").val(res.fecha_hasta);
                $("#formEditarReserva select[name=tipo_habitacion] option[value='"+res.tipo_habitacion+"']").prop('selected', true);

                $("#formEditarReserva #btnGuardarReserva").attr('onclick', 'verificarReservaParaUpdate('+id+', '+res_number+')');
                statusLoading('loadingModalReserva', 0);
            });
        }else{
            showMessage('msgModalEditar', 'alert-danger', "Error al obtener los datos de la reserva");
            statusLoading('loadingModalReserva', 0);
        }
    }).fail(function( jqxhr, textStatus, error ) {
        showMessage('msgModalEditar', 'alert-danger', "Error al obtener los datos de la reserva");
        statusLoading('loadingModalReserva', 0);
    });
}

function verificarReservaParaUpdate(id, res_number){
    //VERIFICAR HABITACIONES PARA RESERVA Y SI SE PUEDE LLAMAR FUNCION PARA UPDATE
    //LOS TIPOS DE HABITACION HARDCODEAR EL VALOR Y ASIGNARLO EN LA PARTE DE ROMTYPE,
    //FROM Y TO DE LAS FECHAS
    //USUARIO Y HOTEL VA HARDCODEADO Q ES EL USUARIO Q ACCEDE A MINI HOTEL Y ID HOTEL EL NOMBRE DEL HOTEL EN SETTING
    //EN PRICES PONER UNO POR DEFECTO
    //PARA Q CUANDO SE MANDE EL XML YA VAYA CONFIGURADO DE ACUERDO AL FORM

    var url = "http://api.minihotelpms.com/GDS";

    var tipo_habitacion = $("#formEditarReserva select[name=tipo_habitacion]").val();
    var fecha_desde = $("#formEditarReserva input[name=fecha_desde]").val();
    var fecha_hasta = $("#formEditarReserva input[name=fecha_hasta]").val();

    //Validar formulario
    var array_checkForm = checkFormShowingMsg('formEditarReserva');
    var msg = '';
    if (array_checkForm['puedeGuardar'] == false){
        msg = array_checkForm['msg'];
        showMessage('msgModalEditar', 'alert-warning', msg);
        // showMessageModal(msg);
        return false;
    }

    //XML DE VERIFICACION
    var xml_verify = '' +
        '<?xml version="1.0" encoding="UTF-8" ?>' +

        '<AvailRaterq xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">'+

            // '<Authentication username="TestTours" password="2222" />'+
            '<Authentication username="GivenUser" password="GivenPassword"/>'+
            '<Hotel id="testhotel" Currency="ILS"/>'+

            '<DateRange from="'+fecha_desde+'" to="'+fecha_hasta+'" />'+
            '<Guests adults="1" child="0" babies="0" />'+
            '<RoomTypes>'+
                '<RoomType id="'+tipo_habitacion+'" />'+
            '</RoomTypes>'+

            '<Prices rateCode="*ALL">'+
                '<Price boardCode="RO" />'+
                '<Price boardCode="BF" />'+
                '<Price boardCode="HBL"/>'+
                '<Price boardCode="HBD"/>'+
            '</Prices>'+

        '</AvailRaterq>';

    //LLAMADA A WEB SERVICE PARA VERIFICAR
    $.support.cors = true;
    $.ajax({
        url: url,
        type: 'POST',
        crossDomain: true,
        data: xml_verify,
        dataType: 'text',
        username: 'TestTours',
        password: '2222',
        beforeSend: function () {
            $("#btnGuardarReserva").html("<img width='25' src='images/Rolling.gif'></img> Verificando ..");
        },
        success: function (response) {
            var error_code = response.substring(4, 7);
            var error_msg = response.substring(9, 999);

            //LEER LA RESPUESTA Y BUSCAR CODIGOS DE ERROR SI SE ENCUENTRAN MOSTRARLOS
            //SINO SE ENCUENTRAN LLAMAR REGISTRO RESERVA PASANDO URL
            //CADA ERROR REPRESENTA UN MENSAJE
            msg_error_formatted = formatMsgWithErrorCodeAndMsg(error_code, error_msg);

            //SI DEVUELVE XML BUSCAMOS LOS ROOMS PARA MOSTRAR VALOR
            if (msg_error_formatted == false){

                //Pasar String XML a Objeto XML
                var xmlDoc = parsearXml(response);

                //Leer Objeto XML con respuesta web service
                if (xmlDoc) {
                    //Si no encuentra roomtype en el xml significa que no hay habitaciones disponibles para ese rango de fecha y habitacion
                    if (typeof(xmlDoc.getElementsByTagName("RoomType")[0]) != 'undefined'){
                        //Nombre
                        var roomType = xmlDoc.getElementsByTagName("RoomType")[0];
                        var attr_Name_e = roomType.getAttributeNode("Name_e");
                        var value_name_e = attr_Name_e.nodeValue;

                        //Valor por dia
                        var roomType_inventory = xmlDoc.getElementsByTagName("Inventory")[0];
                        var attr_maxavail = roomType_inventory.getAttributeNode("maxavail");
                        var value_max_avail = attr_maxavail.nodeValue;

                        //Habitaciones Disponibles
                        var roomType_price = xmlDoc.getElementsByTagName("price")[0];
                        var attr_price_x_night = roomType_price.getAttributeNode("value");
                        var value_price_x_night = attr_price_x_night.nodeValue;

                        if (confirm(value_max_avail + ' Habitaciones Disponibles \n \n'+
                            '¿Confirmar Reserva de Habitación?\n\n'+
                            'Datos: \n'+
                            '-Tipo Habitación: '+value_name_e+' \n'+
                            '-Reservación desde '+formatDateTime(fecha_desde, 'd-m-Y')+' Hasta '+formatDateTime(fecha_hasta, 'd-m-Y')+' \n'+
                            '-Precio por noche: $'+ value_price_x_night)){

                            showMessage('msgModalEditar', 'alert-success', 'Hay disponibilidad para modificar la reserva .. Guardando <img width="30" src="images/Rolling.gif"></img>');

                            updateReserva(url);
                        }else{
                            $("#btnGuardarReserva").html("Guardar");
                            return false;
                        }

                    }else{
                        showMessage('msgModalEditar', 'alert-danger', 'No hay habitaciones disponible en ese rango de fecha y tipo de habitación');
                        $("#btnGuardarReserva").html("Guardar");
                    }
                }

            //Si encuentra roomtype ahi si registra sino no hay habitaicones disponibles
            }else{
                showMessage('msgModalEditar', 'alert-danger', msg_error_formatted);
                $("#btnGuardarReserva").html("Guardar");
            }
        },
        error: function (jqXHR, tranStatus, errorThrown) {
            alert(
                'Status: ' + jqXHR.status + ' ' + jqXHR.statusText + '. ' +
                'Response: ' + jqXHR.responseText
            );
            $("#btnGuardarReserva").html("Guardar");
        }
    });
}

function updateReserva(url){
    var nombre = $("#formEditarReserva input[name=name]").val();
    var apellido = $("#formEditarReserva input[name=lastname]").val();
    var dni = $("#formEditarReserva input[name=dni]").val();
    var tipo_habitacion = $("#formEditarReserva select[name=tipo_habitacion]").val();
    var fecha_desde = $("#formEditarReserva input[name=fecha_desde]").val();
    var fecha_hasta = $("#formEditarReserva input[name=fecha_hasta]").val();

    //Dia de hoy formateado
    // var date = new Date();
    // var dateDB = date.getFullYear() +'-'+ (parseFloat(date.getMonth()) + 1) + '-' + date.getDate();
    // var dateToday = formatDateTime(dateDB, 'd-m-Y');

    //UPDATE RESERVA
    var xml_register = '' +
        '<?xml version="1.0" encoding="utf-8" ?>'+
        '<UpdateBookingInfoRQ TimeStamp="2012-12-10T21:51:15" Version="1.1" '+
            'Target="Production" EchoToken="UserTokenToBeReturned">'+
            '<Authentication Username="test" Password="2222" />'+
            '<Hotel id="ilantest" />'+
            // '<Hotel id="testhotel" />'+
            '<Bookings>'+
                '<Booking MiniHotelBookingID="7003233" PortalBookingID="87654321"/>'+
            '</Bookings>'+
        '</UpdateBookingInfoRQ>'
}
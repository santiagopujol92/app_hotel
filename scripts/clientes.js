jQuery(document).ready(function($) {
	getUsers();
});

function getUsers(){
    var url = root_project + "php/abm_users.php?action=select&jsoncallback=?";
 	var id_user = localStorage.getItem('id_user');
    var type_user = localStorage.getItem('type');

 	//Dia de hoy formateado
	// var date = new Date();
	// var dateDB = date.getFullYear() +'-'+ (parseFloat(date.getMonth()) + 1) + '-' + date.getDate();
    // 
    var filtro_cliente = $("#formBuscarClientes input[name=cliente]").val();
    $.getJSON(url,
        {
            "id_user":id_user,
            "type_user":type_user,
            "filtro_cliente":filtro_cliente
            // "dateToday":dateDB
        })
    .done(function(data) { 
        $("#tbody_clientes").empty();
        statusLoading('loadingClientes', 1 , 'Cargando ..');
        if (data){
        	$.each(data, function(index, res) {
                if (res.responsable_cuenta == null){
                    res.responsable_cuenta = '';
                }

                var boton_activar_admin = '';
                if (type_user == 3){
                    if (res.type != 3){
                        //Admin
                        if (res.type == 1){
                            boton_activar_admin = '<a type="button" style="margin-top:4px;" title="Cambiar Tipo de Usuario" class="btn btn-success"  onclick="abrirModalCambiarTipoUsuario('+res.id+', 2)" data-toggle="modal" data-target="#modalTipoDeUsuario" data-whatever="@mdo">'+
                                '<i class="glyphicon glyphicon-user" aria-hidden="true"></i> Admin'+
                            '</a>';
                        //Cliente
                        }else if(res.type == 2){
                            boton_activar_admin = '<a type="button" style="margin-top:4px;" title="Cambiar Tipo de Usuario" class="btn btn-danger"  onclick="abrirModalCambiarTipoUsuario('+res.id+', 1)" data-toggle="modal" data-target="#modalTipoDeUsuario" data-whatever="@mdo">'+
                                '<i class="glyphicon glyphicon-user" aria-hidden="true"></i> Cliente'+
                            '</a>';
                        }
                    }
                }

        		$("#tbody_clientes").append(
            		'<tr class="hovered">'+
                        '<td>'+res.name+' '+res.lastname+'</td>'+
						'<td>'+res.dni+'</td>'+
						'<td>'+res.telefono+'</td>'+
                        '<td>'+res.email+'</td>'+
                        '<td>'+res.preferencia+'</td>'+
                        '<td>'+res.responsable_cuenta+'</td>'+ //traer el nombre de la cuenta correinte
						'<td>'+formatDateTime(res.ultima_fecha_reserva, 'd-m-Y')+'</td>'+
						'<td>'+
							'<a type="button" style="margin-top:4px;" title="Editar Preferencia" class="btn btn-warning" data-toggle="modal" data-target="#modalEditarPreferencia" onclick="abrirModalPreferencia('+res.id+')" data-whatever="@mdo">'+
								'<i class="glyphicon glyphicon-star" aria-hidden="true"></i>'+
							'</a>&nbsp;'+
                            '<a type="button" style="margin-top:4px;" title="Cuenta Corriente" class="btn btn-info"  onclick="abrirModalCuentaCorriente('+res.id+')" data-toggle="modal" data-target="#modalCuentaCorriente" data-whatever="@mdo">'+
                                '<i class="glyphicon glyphicon-briefcase" aria-hidden="true"></i>'+
                            '</a>&nbsp;'+
                            '<a type="button" style="margin-top:4px;" title="Ver Todas Reservas" class="btn btn-primary"  onclick="abrirModalReservas('+res.id+')" data-toggle="modal" data-target="#modalReservas" data-whatever="@mdo">'+
                                '<i class="glyphicon glyphicon-th-list" aria-hidden="true"></i>'+
                            '</a>&nbsp;'+
                            '<a type="button" style="margin-top:4px;" title="Enviar Mensaje a Cliente" class="btn btn-success"  onclick="abrirModalMensaje('+res.id+')" data-toggle="modal" data-target="#modalMensaje" data-whatever="@mdo">'+
                                '<i class="glyphicon glyphicon-send" aria-hidden="true"></i>'+
                            '</a>&nbsp;'+
                            boton_activar_admin+
						'</td>'+
					'</tr>'	
				);
        	});
            
            statusLoading('loadingClientes', 0);
            $("#total_resultado").html(data.length);
        }else{  
        	showMessage('messageClientesList', 'alert-warning', "No hay clientes");
            statusLoading('loadingClientes', 0);
        }
    }).fail(function( jqxhr, textStatus, error ) {
        showMessage('messageClientesList', 'alert-danger', "Error al obtener los clientes o acceso denegado");
        statusLoading('loadingClientes', 0);
    });
}


function abrirModalPreferencia(id){

    var url = root_project + "php/abm_users.php?action=select&jsoncallback=?";
    statusLoading('loadingmodalEditarPreferencia', 1 , 'Cargando ..');
    var type_user = localStorage.getItem('type');
    clearForm('formPreferencia');

    $.getJSON(url,
        {
            "id":id,
            "type_user":type_user,
        })
    .done(function(data) { 
        if (data){
            $.each(data, function(index, res) {
                $("#formPreferencia [name=preferencia]").val(res.preferencia);
                $("#formPreferencia #btnGuardarPreferencia").attr('onclick', 'guardarPreferencia('+id+')');
                statusLoading('loadingmodalEditarPreferencia', 0);
            });
        }else{
            showMessage('msgModalEditarPreferencia', 'alert-danger', "Error al obtener datos del cliente");
            statusLoading('loadingmodalEditarPreferencia', 0);
        }
    }).fail(function( jqxhr, textStatus, error ) {
        showMessage('msgModalEditarPreferencia', 'alert-danger', "Error al obtener datos del cliente");
        statusLoading('loadingmodalEditarPreferencia', 0);
    });
}


function guardarPreferencia(id){
 var url = root_project + "php/abm_users.php?action=update_preferencia&jsoncallback=?";
 var preferencia = $("#formPreferencia [name=preferencia]").val();
 $("#btnGuardarPreferencia").html('<img width="15px" style="margin-bottom:5px;" src="images/Rolling.gif"/> Guardando');
 $.getJSON(url,
        {
            'id_client' : id,
            'preferencia' : preferencia,
        })
    .done(function(respuesta) { 
        showMessageModal('Preferencia modificada con éxito', 'Bien !', 'text-success', 'success');
        $("#btnGuardarPreferencia").html('Guardar');
        $("#modalEditarPreferencia").modal('hide');
        getUsers();
    }).fail(function( jqxhr, textStatus, error ) {
        console.log(
            'Status: ' + jqxhr + ' ' + textStatus + '. ' +
            'Response: ' + error
        );
        $("#btnGuardarPreferencia").html('Guardar');
        showMessageModal('Error al guardar la preferencia', 'Error !', 'text-danger', 'danger');
    });
}


function abrirModalCuentaCorriente(id){

    var url = root_project + "php/abm_cuenta_corriente.php?action=select&jsoncallback=?";
    statusLoading('loadingmodalCuentaCorriente', 1 , 'Cargando ..');
    var type_user = localStorage.getItem('type');

    clearForm('formCuentaCorriente');
    $("#formCuentaCorriente input[name=status_cuenta_corriente]").val('0');
    $("#modalCuentaCorrienteTitle").html('Cuenta Corriente');

    $.getJSON(url,
        {
            "id":id,
            "type_user":type_user,
        })
    .done(function(data) { 

        $("#formCuentaCorriente #btnGuardarCuentaCorriente").attr('onclick', 'guardarCuentaCorriente('+id+')');
        $("#formCuentaCorriente #btnEliminarCuentaCorriente").attr('onclick', 'eliminarCuentaCorriente('+id+')');

        if (data){
            $.each(data, function(index, res) {
                $("#modalCuentaCorrienteTitle").html('Cuenta Corriente - <b>' + res.name + " " + res.lastname + '</b>');

                $("#formCuentaCorriente input[name=responsable]").val(res.responsable);
                $("#formCuentaCorriente input[name=empresa]").val(res.empresa);
                $("#formCuentaCorriente select[name=tipo_cuenta]").val(res.tipo_cuenta);
                $("#formCuentaCorriente input[name=status_cuenta_corriente]").val(res.status_cuenta_corriente);

                statusLoading('loadingmodalCuentaCorriente', 0);
            });
        }else{
            showMessage('msgModalCuentaCorriente', 'alert-info', "Este cliente no tiene cuenta corriente asignada");
            statusLoading('loadingmodalCuentaCorriente', 0);
        }
    }).fail(function( jqxhr, textStatus, error ) {
        showMessage('msgModalCuentaCorriente', 'alert-danger', "Error al obtener datos de la cuenta");
        statusLoading('loadingmodalCuentaCorriente', 0);
    });
}

function guardarCuentaCorriente(id){
    var url = root_project + "php/abm_cuenta_corriente.php?action=insert_update_cuenta_corriente&jsoncallback=?";

    var responsable = $("#formCuentaCorriente input[name=responsable]").val();
    var empresa = $("#formCuentaCorriente input[name=empresa]").val();
    var tipo_cuenta = $("#formCuentaCorriente select[name=tipo_cuenta]").val();
    var status_cuenta_corriente = $("#formCuentaCorriente input[name=status_cuenta_corriente]").val();

    array_check = checkFormShowingMsg('formCuentaCorriente');
    if (array_check['puedeGuardar']){
        $("#btnGuardarCuentaCorriente").html('<img width="15px" style="margin-bottom:5px;" src="images/Rolling.gif"/> Guardando');
        $.getJSON(url,
            {
                'id_client' : id,
                'responsable' : responsable,
                'empresa' : empresa,
                'tipo_cuenta' : tipo_cuenta,
                'status_cuenta_corriente' : status_cuenta_corriente
            })
        .done(function(respuesta) { 
            showMessageModal('Cuenta corriente actualizada con éxito', 'Bien !', 'text-success', 'success');
            $("#btnGuardarCuentaCorriente").html('Guardar');
            $("#modalCuentaCorriente").modal('hide');
            getUsers();
        }).fail(function( jqxhr, textStatus, error ) {
            console.log(
                'Status: ' + jqxhr + ' ' + textStatus + '. ' +
                'Response: ' + error
            );
            $("#btnGuardarCuentaCorriente").html('Guardar');
            showMessageModal('Error al guardar la cuenta corriente', 'Error !', 'text-danger', 'danger');
        });
    }else{
        showMessageModal(array_check['msg']);
        return false;
    }

}


function eliminarCuentaCorriente(id){
    var url = root_project + "php/abm_cuenta_corriente.php?action=delete&jsoncallback=?";

    $("#btnEliminarCuentaCorriente").html('<img width="15px" style="margin-bottom:5px;" src="images/Rolling.gif"/> Eliminando');
    $.getJSON(url,
        {
            'id_client' : id,
        })
    .done(function(respuesta) { 
        showMessageModal('Cuenta corriente eliminada con éxito', 'Bien !', 'text-success', 'success');
        $("#btnEliminarCuentaCorriente").html('Eliminar');
        $("#modalCuentaCorriente").modal('hide');
        getUsers();
    }).fail(function( jqxhr, textStatus, error ) {
        console.log(
            'Status: ' + jqxhr + ' ' + textStatus + '. ' +
            'Response: ' + error
        );
        $("#btnEliminarCuentaCorriente").html('Eliminar');
        showMessageModal('Error al eliminar la cuenta corriente', 'Error !', 'text-danger', 'danger');
    });

}

//Abrir modal con reservas del cliente usuario
function abrirModalReservas(id){
    getReservasUser(id);
}

//Obtener las reservas y renderizarlas a la tabla del modal
function getReservasUser(id){
    var url = root_project + "php/abm_reserva.php?action=select&jsoncallback=?";
    $("#tbody_reservas").html('');
    $.getJSON(url,
        {
            "id_user":id,
        })
    .done(function(data) { 
        if (data){
            $.each(data, function(index, res) {
                var tipo_habitacion = formatShowTipoHabitacion(res.tipo_habitacion);

                $("#tbody_reservas").append(
                    '<tr style="font-size: 12px;" class="hovered">'+
                        '<th scope="row">'+res.res_number+'</th>'+
                        '<td>'+name_user+' '+lastname_user+'</td>'+
                        '<td>'+res.name+' '+res.lastname+'</td>'+
                        '<td>'+res.dni+'</td>'+
                        '<td>'+tipo_habitacion+'</td>'+
                        '<td>'+formatDateTime(res.fecha_desde, 'd-m-Y')+'</td>'+
                        '<td>'+formatDateTime(res.fecha_hasta, 'd-m-Y')+'</td>'+
                    '</tr>' 
                );
            });
            
        }else{  
            showMessage('messageReservasList', 'alert-warning', "No hay reservas realizadas");
        }
    }).fail(function( jqxhr, textStatus, error ) {
        showMessage('messageReservasList', 'alert-danger', "Error al obtener las reservas");
    });
}

function abrirModalMensaje(id){

    var url = root_project + "php/abm_users.php?action=select&jsoncallback=?";
    statusLoading('loadingmodalMensaje', 1 , 'Cargando ..');
    var type_user = localStorage.getItem('type');
    clearForm('formMensaje');

    $.getJSON(url,
        {
            "id":id,
            "type_user":type_user,
        })
    .done(function(data) { 
        if (data){
            $.each(data, function(index, res) {
                $("#formMensaje [name=mensaje]").val(res.mensaje);
                $("#formMensaje #btnGuardarMensaje").attr('onclick', 'guardarMensaje('+id+')');
                statusLoading('loadingmodalMensaje', 0);
            });
        }else{
            showMessage('msgmodalMensaje', 'alert-danger', "Error al obtener datos del cliente");
            statusLoading('loadingmodalMensaje', 0);
        }
    }).fail(function( jqxhr, textStatus, error ) {
        showMessage('msgmodalMensaje', 'alert-danger', "Error al obtener datos del cliente");
        statusLoading('loadingmodalMensaje', 0);
    });
}

function guardarMensaje(id){
    var url = root_project + "php/abm_users.php?action=update_mensaje&jsoncallback=?";
    var mensaje = $("#formMensaje [name=mensaje]").val();
    array_check = checkFormShowingMsg('formMensaje');
    if (array_check['puedeGuardar']){
        $("#btnGuardarMensaje").html('<img width="15px" style="margin-bottom:5px;" src="images/Rolling.gif"/> Guardando');
        $.getJSON(url,
            {
                'id_client' : id,
                'mensaje' : mensaje,
            })
        .done(function(respuesta) { 
            showMessageModal('Mensaje guardado con éxito', 'Bien !', 'text-success', 'success');
            $("#btnGuardarMensaje").html('Guardar');
            $("#modalMensaje").modal('hide');
            getUsers();
        }).fail(function( jqxhr, textStatus, error ) {
            console.log(
                'Status: ' + jqxhr + ' ' + textStatus + '. ' +
                'Response: ' + error
            );
            $("#btnGuardarMensaje").html('Guardando');
            showMessageModal('Error al guardar el mensaje', 'Error !', 'text-danger', 'danger');
        });
    }else{
        showMessageModal(array_check['msg']);
        return false;
    }
}

function abrirModalCambiarTipoUsuario(id, id_type_user){
    if (id_type_user != 3){
        //id_type_user es el id a donde se quiere cambiar el usuario
        if (id_type_user == 1){
            $("#type_user_text").html('Admin');
        }else if (id_type_user == 2){
            $("#type_user_text").html('Cliente')
        }

        $("#btnConfirmarTipoDeUsuario").attr('onclick', 'updateTypeUser('+id+', '+id_type_user+')');
    }
}

function updateTypeUser(id, id_type_user){
    if (id_type_user != 3){
         var url = root_project + "php/abm_users.php?action=update_type_user&jsoncallback=?";
         $("#btnConfirmarTipoDeUsuario").html('<img width="15px" style="margin-bottom:5px;" src="images/Rolling.gif"/> Guardando');
         var type_user_logged = localStorage.getItem('type');
         $.getJSON(url,
                {
                    'type_user_logged' : type_user_logged,
                    'id_client' : id,
                    'id_type_user' : id_type_user,
                })
            .done(function(respuesta) { 
                showMessageModal('Tipo de usuario modificado con éxito', 'Bien !', 'text-success', 'success');
                $("#btnConfirmarTipoDeUsuario").html('Confirmar');
                $("#modalTipoDeUsuario").modal('hide');
                getUsers();
            }).fail(function( jqxhr, textStatus, error ) {
                console.log(
                    'Status: ' + jqxhr + ' ' + textStatus + '. ' +
                    'Response: ' + error
                );
                $("#btnConfirmarTipoDeUsuario").html('Confirmar');
                showMessageModal('Error al modificar el tipo de usuario', 'Error !', 'text-danger', 'danger');
            });
    }
}